# Copyright (c) 2023-2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("../../../device_status.gni")

config("interaction_drag_public_config") {
  include_dirs = [ "include",
                   "//base/msdp/device_status/interfaces/innerkits/interaction/include", ]
}

if (!is_arkui_x) {
ohos_shared_library("interaction_drag") {
  sanitize = {
    integer_overflow = true
    ubsan = true
    boundary_sanitize = true
    cfi = true
    cfi_cross_dso = true
    debug = false
  }

  branch_protector_ret = "pac_ret"

  include_dirs = [
    "include",
    "${device_status_interfaces_path}/innerkits/interaction/include",
    "${device_status_utils_path}/include",
    "${device_status_service_path}/context/include",
    "${device_status_service_path}/interaction/drag/include",
    "${device_status_service_path}/native/include",
    "//third_party/cJSON",
  ]

  sources = [
    "src/display_change_event_listener.cpp",
    "src/drag_data_manager.cpp",
    "src/drag_drawing.cpp",
    "src/drag_hisysevent.cpp",
    "src/drag_manager.cpp",
    "src/drag_smooth_processor.cpp",
    "src/drag_vsync_station.cpp",
    "src/event_hub.cpp",
    "src/state_change_notify.cpp",
  ]

  defines = device_status_default_defines

  public_configs = [ ":interaction_drag_public_config" ]

  if (defined(use_rosen_drawing) && use_rosen_drawing) {
    defines += [ "USE_ROSEN_DRAWING" ]
  }

  deps = [
    "${device_status_root_path}/etc/drag_icon:device_status_drag_icon",
    "${device_status_root_path}/utils/ipc:devicestatus_ipc",
    "${device_status_utils_path}:devicestatus_util",
    "//third_party/libxml2:xml2",
  ]

  public_deps =
      [ "${device_status_root_path}/intention/prototype:intention_prototype" ]

  external_deps = [
    "ability_base:want",
    "cJSON:cjson",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "device_info_manager:distributed_device_profile_common",
    "device_info_manager:distributed_device_profile_sdk",
    "device_manager:devicemanagersdk",
    "dsoftbus:softbus_client",
    "eventhandler:libeventhandler",
    "graphic_2d:libcomposer",
    "graphic_2d:librender_service_base",
    "graphic_2d:librender_service_client",
    "graphic_2d:window_animation",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "init:libbegetutil",
    "input:libmmi-client",
    "ipc:ipc_single",
    "qos_manager:concurrent_task_client",
    "qos_manager:qos",
    "samgr:samgr_proxy",
    "udmf:udmf_client",
    "window_manager:libdm",
    "window_manager:libwm_lite",
  ]

  subsystem_name = "${device_status_subsystem_name}"
  part_name = "${device_status_part_name}"
}
} else {
ohos_source_set("interaction_client_crossplatform") {
    include_dirs = [
      "//third_party/cJSON",
      "//base/msdp/device_status/interfaces/innerkits/interaction/include",
      "//base/msdp/device_status/intention/prototype/include",
      "//base/msdp/device_status/services/interaction/drag/include",
      "//foundation/appframework/window_manager/interfaces/innerkits/dm",
      "//commonlibrary/c_utils/base/include",
      "//plugins/multimedia/media/mock",
      "//base/hiviewdfx/hilog/interfaces/native/innerkits/include",
      "${device_status_root_path}/utils/common",
      "${device_status_root_path}/utils/common/include",
      "//foundation/appframework/window_manager/dm/include",
      "//foundation/appframework/window_manager/interfaces/innerkits/dm",
      "//foundation/arkui/ace_engine/frameworks",
      "//foundation/arkui/ace_engine/adapter/android/entrance/java/jni",
      "//foundation/appframework/window_manager/interfaces/innerkits/wm",
      "//foundation/appframework/window_manager/utils/include",
    ]

    sources = [
      "//base/msdp/device_status/frameworks/native/interaction/src/interaction_manager.cpp",
      "src/drag_data_manager.cpp",
      "src/drag_drawing.cpp",
      "src/drag_smooth_processor.cpp",
      "src/drag_manager.cpp",
      "${device_status_root_path}/utils/common/src/util.cpp",
      "${device_status_root_path}/utils/common/src/utility.cpp",
      "//base/msdp/device_status/utils/common/src/animation_curve.cpp",
    ]

    defines = device_status_default_defines

    if (target_os == "android") {
        defines += [ "ANDROID_PLATFORM" ]
    } else if (target_os == "ios") {
        defines += [ "IOS_PLATFORM" ]
    }

    public_configs = [ ":interaction_drag_public_config" ]

    if (defined(use_rosen_drawing) && use_rosen_drawing) {
      defines += [ "USE_ROSEN_DRAWING" ]
    }

    deps = [
      "${device_status_root_path}/etc/drag_icon:device_status_drag_icon",
      "//third_party/libxml2:xml2",
      "//third_party/cJSON:cjson",
      "//foundation/multimedia/image_framework/interfaces/innerkits:image_native",
      "//foundation/appframework/window_manager/dm:dm",
      "//foundation/multimodalinput/input/frameworks/proxy:libmmi-client-crossplatform",
      "//foundation/graphic/graphic_2d/rosen/modules/render_service_client:librender_service_client_static",
    ]
}
}
